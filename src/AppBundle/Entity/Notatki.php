<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Notatki
 */
class Notatki
{
    /**
     * @var string
     */
    private $tresc;

    /**
     * @var \DateTime
     */
    private $dataDodania;

    /**
     * @var boolean
     */
    private $wykreslona;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set tresc
     *
     * @param string $tresc
     * @return Notatki
     */
    public function setTresc($tresc)
    {
        $this->tresc = $tresc;

        return $this;
    }

    /**
     * Get tresc
     *
     * @return string 
     */
    public function getTresc()
    {
        return $this->tresc;
    }

    /**
     * Set dataDodania
     *
     * @param \DateTime $dataDodania
     * @return Notatki
     */
    public function setDataDodania($dataDodania)
    {
        $this->dataDodania = $dataDodania;

        return $this;
    }

    /**
     * Get dataDodania
     *
     * @return \DateTime 
     */
    public function getDataDodania()
    {
        return $this->dataDodania;
    }

    /**
     * Set wykreslona
     *
     * @param boolean $wykreslona
     * @return Notatki
     */
    public function setWykreslona($wykreslona)
    {
        $this->wykreslona = $wykreslona;

        return $this;
    }

    /**
     * Get wykreslona
     *
     * @return boolean 
     */
    public function getWykreslona()
    {
        return $this->wykreslona;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}

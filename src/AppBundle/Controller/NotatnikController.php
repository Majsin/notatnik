<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Notatki;

class NotatnikController extends Controller
{
	public function formularzAction(Request $request)
    {
		return $this->render('AppBundle:Notatnik:formularz.html.twig');
    }
	
    public function dodanoAction(Request $request)
    {
		$em = $this->getDoctrine()->getManager();
		
		$postData = $request->request->all();
		$notatki = new Notatki();
		print($postData['tresc']);
		$notatki->setTresc($postData['tresc']);
		$nowDateTime = new \DateTime();
		$notatki->setDataDodania($nowDateTime);
		$notatki->setWykreslona(false);
		
		$em->persist($notatki);
		$em->flush();
		
		return $this->render('AppBundle:Notatnik:dodano.html.twig');
    }
	
	public function wyswietlAction(Request $request)
    {
		$notatkiRepository = $this->getDoctrine()->getRepository('AppBundle:Notatki');
		$notatki = array(
			'notatki' => $notatkiRepository->findAll()
		);
		return $this->render('AppBundle:Notatnik:wyswietl.html.twig',$notatki);
    }
	
	public function wykreslAction(Request $request, $notatkaId)
    {
		$em = $this->getDoctrine()->getManager();
		$notatkiRepository = $this->getDoctrine()->getRepository('AppBundle:Notatki');
		$notatka = $notatkiRepository->findOneById($notatkaId);
		if (!$notatka) {
			throw $this->createNotFoundException(
				'Brak notatki o id: '.$id
			);
		}
		$notatka->setWykreslona(true);
		$em->flush();

		return $this->redirectToRoute('wyswietl');
    }
	
	public function odkreslAction(Request $request, $notatkaId)
    {
		$em = $this->getDoctrine()->getManager();
		$notatkiRepository = $this->getDoctrine()->getRepository('AppBundle:Notatki');
		$notatka = $notatkiRepository->findOneById($notatkaId);
		if (!$notatka) {
			throw $this->createNotFoundException(
				'Brak notatki o id: '.$id
			);
		}
		$notatka->setWykreslona(false);
		$em->flush();
		
		return $this->redirectToRoute('wyswietl');
    }
}
